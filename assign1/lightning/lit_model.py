import os
import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from assign1.main import categorical_accuracy
from torchtext import data
from torchtext import datasets


class LitBiLSTMPOSTagger(pl.LightningModule):
    def __init__(
            self,
            embedding_dim,
            hidden_dim,
            n_layers,
            bidirectional,
            dropout,
            batch_size,
            data_dir,
            lang,
    ):
        super().__init__()

        self.batch_size = batch_size
        self.lang = lang
        self.train_iterator, self.valid_iterator, self.test_iterator, input_dim, output_dim, pad_idx, self.tag_pad_idx, self.tag_unk_idx = self.load_data(
            data_dir)

        self.embedding = nn.Embedding(input_dim, embedding_dim, padding_idx=pad_idx)

        self.lstm = nn.LSTM(
            embedding_dim,
            hidden_dim,
            num_layers=n_layers,
            bidirectional=bidirectional,
            dropout=dropout if n_layers > 1 else 0,
        )

        self.fc = nn.Linear(hidden_dim * 2 if bidirectional else hidden_dim, output_dim)

        self.dropout = nn.Dropout(dropout)

        self.apply(init_weights)
        self.embedding.weight.data[pad_idx] = torch.zeros(embedding_dim)

        self.epoch_correct = 0
        self.epoch_n_label = 0

    def forward(self, text):
        # text = [sent len, batch size]

        # pass text through embedding layer
        embedded = self.dropout(self.embedding(text))

        # embedded = [sent len, batch size, emb dim]

        # pass embeddings into LSTM
        outputs, (hidden, cell) = self.lstm(embedded)

        # outputs holds the backward and forward hidden states in the final layer
        # hidden and cell are the backward and forward hidden and cell states at the final time-step

        # output = [sent len, batch size, hid dim * n directions]
        # hidden/cell = [n layers * n directions, batch size, hid dim]

        # we use our outputs to make a prediction of what the tag should be
        predictions = self.fc(self.dropout(outputs))

        # predictions = [sent len, batch size, output dim]

        return predictions

    def training_step(self, batch, batch_idx):
        text = batch.text
        tags = batch.udtags
        predictions = self(text)

        # TODO: figure out what this means
        predictions = predictions.view(-1, predictions.shape[-1])
        tags = tags.view(-1)

        # TODO: check if I need criterion: ignore_index=TAG_PAD_IDX
        loss = F.cross_entropy(predictions, tags)
        return {"loss": loss}

    def configure_optimizers(self):
        return optim.Adam(self.parameters())

    def load_data(self, data_dir):
        TEXT = data.Field(lower=True)
        UD_TAGS = data.Field()

        fields = (("text", TEXT), ("udtags", UD_TAGS))

        # load the data from the specific path
        train_data, valid_data, test_data = datasets.UDPOS.splits(
            fields=fields,
            path=os.path.join(data_dir, self.lang),
            train="{}-ud-train.conll".format(self.lang),
            validation="{}-ud-dev.conll".format(self.lang),
            test="{}-ud-test.conll".format(self.lang),
        )
        # building the vocabulary for both text and the labels
        MIN_FREQ = 2
        TEXT.build_vocab(train_data, min_freq=MIN_FREQ)
        UD_TAGS.build_vocab(train_data)
        pad_idx = TEXT.vocab.stoi[TEXT.pad_token]

        tag_pad_idx = UD_TAGS.vocab.stoi[UD_TAGS.pad_token]
        tag_unk_idx = UD_TAGS.vocab.unk_index

        input_dim = len(TEXT.vocab)
        output_dim = len(UD_TAGS.vocab)

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        train_iterator, valid_iterator, test_iterator = data.BucketIterator.splits(
            (train_data, valid_data, test_data),
            batch_size=self.batch_size,
            device=device,
        )

        return train_iterator, valid_iterator, test_iterator, input_dim, output_dim, pad_idx, tag_pad_idx, tag_unk_idx

    def train_dataloader(self):
        return self.train_iterator

    def validation_step(self, batch, batch_idx):
        text = batch.text
        tags = batch.udtags
        predictions = self(text)

        # TODO: figure out what this means
        predictions = predictions.view(-1, predictions.shape[-1])
        tags = tags.view(-1)

        # TODO: check if I need criterion: ignore_index=TAG_PAD_IDX
        loss = F.cross_entropy(predictions, tags)
        return {"val_loss": loss}

    def val_dataloader(self):
        return self.valid_iterator

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x["val_loss"] for x in outputs]).mean()
        return {"val_loss": avg_loss}

    def test_dataloader(self):
        return self.test_iterator

    def test_step(self, batch, batch_idx):
        text = batch.text
        tags = batch.udtags
        predictions = self(text)

        predictions = predictions.view(-1, predictions.shape[-1])
        tags = tags.view(-1)

        loss = F.cross_entropy(predictions, tags)
        self.log('test_loss', loss)

        correct, n_labels = categorical_accuracy(
            predictions, tags, self.tag_pad_idx, self.tag_unk_idx
        )

        # TODO: there should be a better way of doing this
        self.epoch_correct += correct.item()
        self.epoch_n_label += n_labels

    def print_test_accuracy(self):
        test_acc = self.epoch_correct / self.epoch_n_label
        print(f"Test Acc: {test_acc * 100:.2f}%")


def init_weights(m):
    for name, param in m.named_parameters():
        nn.init.normal_(param.data, mean=0, std=0.1)
