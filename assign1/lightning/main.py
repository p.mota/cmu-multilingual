import os
import argparse
import json
import random
import numpy as np
import torch

import warnings

from assign1.lightning.lit_model import LitBiLSTMPOSTagger
from pytorch_lightning import Trainer

warnings.filterwarnings("ignore")

# set command line options
parser = argparse.ArgumentParser(description="main.py")
parser.add_argument(
    "--mode",
    type=str,
    choices=["train", "eval"],
    default="train",
    help="Run mode",
)
parser.add_argument(
    "--lang",
    type=str,
    choices=["en", "cs", "es", "ar", "hy", "lt", "af", "ta"],
    default="en",
    help="Language code",
)
parser.add_argument(
    "--model-name",
    type=str,
    default=None,
    help="name of the saved model",
)

parser.add_argument(
    "--model-dir",
    type=str,
    default="../saved_models",
    help="dir to save the models",
)

parser.add_argument(
    "--data-dir",
    type=str,
    default="../data",
    help="dir with train/dev/test data",
)

parser.add_argument(
    "--config",
    type=str,
    default="../config.json",
    help="torch params configuration",
)

args = parser.parse_args()

if not os.path.exists(args.model_dir):
    os.mkdir(args.model_dir)

if args.model_name is None:
    args.model_name = "{}-PL-model".format(args.lang)

# set a fixed seed for reproducibility
SEED = 1234

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

params = json.load(open(args.config))


def main():
    print("Running Pytorch Lightning main.py in {} mode with lang: {}".format(args.mode, args.lang))
    model = LitBiLSTMPOSTagger(
        embedding_dim=params["embedding_dim"],
        hidden_dim=params["hidden_dim"],
        n_layers=params["n_layers"],
        bidirectional=params["bidirectional"],
        dropout=params["dropout"],
        batch_size=params["batch_size"],
        data_dir=args.data_dir,
        lang=args.lang,
    )
    trainer = Trainer(auto_select_gpus=1, max_epochs=params["num_epochs"], fast_dev_run=False)

    if args.mode == "train":
        trainer.fit(model)
        torch.save(
            model.state_dict(), "{}/{}.pt".format(args.model_dir, args.model_name)
        )
    elif args.mode == "eval":
        model.load_state_dict(torch.load("{}/{}.pt".format(args.model_dir, args.model_name)))
        trainer.test(model)
        model.print_test_accuracy()


if __name__ == "__main__":
    main()
